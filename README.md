# Data Preprocessing
## Preprocesamiento de textos

- Eliminar signos de puntuación

- Eliminar emojis

- Tokenización de textos

- Stopo Words

- Convertir a minúsculas

- Representación de textos en one_hot

- Guarda resultado en cada una de las etapas

- Guarda representación de textos

## Parámetros
- Path conjunto de datos
- Path rutas de archivos de salida

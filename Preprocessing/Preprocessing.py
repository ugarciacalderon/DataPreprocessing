
import pandas as pd
import numpy as np
import string
import re

from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from keras.preprocessing.text import Tokenizer
from webencodings import encode


class Preprocessing:
    def __init__(self, path_dataset:str):
        self.path_dataset_train = path_dataset
        self.listIdPost = []
        self.listClasses = []

        self.__openDatasets()

    def __openDatasets(self):
        '''
        abre los archivos y los convierte a Dataframe
        :return:
        '''
        print('Abriendo dataset')
        # open datasets
        # open data train
        path_dataset_train = pd.read_csv(self.path_dataset_train, encoding="cp437")
        dataframe = pd.DataFrame(path_dataset_train)
        self.__getCharacteristics(dataframe)

        return 0

    def __getCharacteristics(self,dataframe):
        '''
        método para obtener carcaterísticas de los conjunto de datos
        :param dataframeTrain:
        :param dataframeValidation:
        :return:
        '''
        print("Convirtiendo a Dataframe")
        # get all idPost
        self.listIdPost = dataframe['idPost']
        # get all classes
        self.listClasses = dataframe['Class']
        # Gets characteristics of train dataset
        dataframeTrain = np.array(dataframe.iloc[:, :])
        #y_train = np.array(dataframeTrain.iloc[:, -1])

        self.__tokenizationAndRemoveStopWords(dataframeTrain)
        #self.__tokenizationAndRemoveStopWordsValidation(dataframeValidation)
        return 0

    def __tokenizationAndRemoveStopWords(self, dataframe):
        '''
        Este método contiene todo el preprocesamiento de texto para el conjunto de entrenamiento
        * tokenización
        * elimina stop words
        * elimina signos de puntuación,emojis, espacios en blanco y convierte a minusculas
        :param dataframeTrain:
        :param dataframeValidation:
        :return:
        '''
        print('Limpiando textos (tokenización,elimina stop words,elimina signos de puntuación etc)')
        # tokeniza X_train
        tokenization_sentences = [word_tokenize(post[1]) for post in dataframe]
        # remove stop words
        stop_words = set(stopwords.words('english'))
        stopwords_sentences_filtered = []
        for sentence in tokenization_sentences:
            stopwords_sentences_filtered.append([word for word in sentence if not word in stop_words])
        # remove punctuations, emojis, whitespaces, lower, elimina cadenas con longitud de 0 y 1
        table = str.maketrans('', '', string.punctuation)
        remove_punctuations_sentences = []
        for sentence in stopwords_sentences_filtered:
            remove_punctuations_sentences.append([re.sub(r'(\b\w{0,1}\b)', '', word.lower()) for word in sentence if word.isalpha()])

        # escribir el conjunto de datos preprocesado tokenizado
        listaTemp = []
        #dataframeResult = pd.DataFrame([idPost,post,clase for idPost,post,clase in zip(dataframeTrain,remove_punctuations_sentences,dataframeTrain)])
        for idPost,post,clase in zip(self.listIdPost,remove_punctuations_sentences,self.listClasses):
            # sentence tiene una lista con tres cadenas
            # identificador post, sentence[0]
            # contenido del post, sentence [1]
            # clase del post, sentence [2]
            listaTemp2 = [[idPost],post,[clase]]
            listaTemp.append(listaTemp2)

        # convertir a una sola lista de listas
        listaFinal = []
        for lista in listaTemp:
            # une sublistas en una sola
            lista = sum(lista, [])
            # elimina cadenas vacías
            lista = [cadena for cadena in lista if cadena]
            listaFinal.append(lista)
        listaTemp.clear()
        # crea dataframe para escribirlo
        with open(f"{self.path_dataset_train[25:-4]}-Preprocessing.txt", 'w') as file:
            for row in listaFinal:
                s = ",".join(map(str, row))
                file.write(s +','+ '\n')
        print('guardo primer etapa de preprocesamiento en dataframePreprocessedTrain.csv')
        # generar caracteristicas
        self.oneHotEncoding(listaFinal)

    def oneHotEncoding(self, sentences):
        '''
        método para la geneación de características haciendo uso de one_hot_encoding
        :param remove_punctuations_sentences: texto preprocesado
        :return:
        '''
        print('Aplicando one_hot_encoding')
        one_hot_encoding = []
        t = Tokenizer()
        for sentence in sentences:
            # print(sentence[1:-1]) toma lo que esta después de 'idPost' y antes de la 'Clase'
            t.fit_on_texts(sentence[1:-1])
            one_hot_encoding.append(t.texts_to_matrix(sentence[1:-1], mode='count'))
        # escribir el resultado en un dataframe
        listaTemp = []
        # dataframeResult = pd.DataFrame([idPost,post,clase for idPost,post,clase in zip(dataframeTrain,remove_punctuations_sentences,dataframeTrain)])
        for idPost,coding,clase in zip(self.listIdPost,one_hot_encoding,self.listClasses):
            # sentence tiene una lista con tres cadenas
            # identificador post, sentence[0]
            # contenido del post, sentence [1]
            # clase del post, sentence [2]
            listaTemp2 = [[idPost],coding.astype(int).tolist(),[clase]]
            # convirete lista de listas a una sola lista
            listaTemp2 = [item for sublist in listaTemp2 for item in sublist]
            listaTemp.append(listaTemp2)

        # crea dataframe para escribirlo
        with open(f"{self.path_dataset_train[25:-4]}-Representation.txt", 'w') as file:
            for row in listaTemp:
                s = ",".join(map(str, row))
                file.write(s.replace('[','').replace(']','') +','+ '\n')




from TABICON37.MySintaxis import *
from Preprocessing.Preprocessing import *
import sys
import os

if __name__ == '__main__':
    args = []

    for element in range(1, len(sys.argv)):
        args.append(sys.argv[element].replace('\\','//'))

    param = MyListArgs(args)
    configFile = param.valueArgsAsString('-CONFIG', '')

    if configFile != '':
        param.addArgsFromFile(configFile)

    sintaxis = '-PATH_DATASET:str'
    # ruta del conjunto de entrenamiento o del conjunto de validación
    PATH_DATASET = param.valueArgsAsString('-PATH_DATASET', '') # ruta del conjunto de entrenamiento
    #'../dataset/english_train/agr_en_dev.csv' conjunto de validación
    print(PATH_DATASET)

    # funcionamiento
    preprocessing = Preprocessing(path_dataset=PATH_DATASET)